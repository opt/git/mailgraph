# Italian (it) translation of debconf templates for mailgraph
# This file is distributed under the same license as the mailgraph package.
# Luca Monducci <luca.mo@tiscali.it>, 2006-2010
#
msgid ""
msgstr ""
"Project-Id-Version: mailgraph 1.14 italian debconf templates\n"
"Report-Msgid-Bugs-To: mailgraph@packages.debian.org\n"
"POT-Creation-Date: 2010-10-12 06:32+0200\n"
"PO-Revision-Date: 2010-08-20 16:01+0200\n"
"Last-Translator: Luca Monducci <luca.mo@tiscali.it>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../mailgraph.templates:1001
msgid "Should Mailgraph start on boot?"
msgstr "Far partire Mailgraph all'avvio?"

#. Type: boolean
#. Description
#: ../mailgraph.templates:1001
msgid ""
"Mailgraph can start on boot time as a daemon. Then it will monitor your "
"Postfix logfile for changes. This is recommended."
msgstr ""
"Mailgraph pu? essere fatto partire all'avvio come demone. Poi controlla le "
"modifiche al file di log di Postfix. Questo ? il metodo raccomandato."

#. Type: boolean
#. Description
#: ../mailgraph.templates:1001
msgid "The other method is to call mailgraph by hand with the -c parameter."
msgstr "L'altro metodo ? richiamare manualmente mailgraph con il parametro -c."

#. Type: string
#. Description
#: ../mailgraph.templates:2001
msgid "Logfile used by mailgraph:"
msgstr "File di log per mailgraph:"

#. Type: string
#. Description
#: ../mailgraph.templates:2001
msgid ""
"Enter the logfile which should be used to create the databases for "
"mailgraph. If unsure, leave default (/var/log/mail.log)."
msgstr ""
"Inserire il file di log che mailgraph deve usare per creare i database. Se "
"non si ? sicuri lasciare il valore predefinito (/var/log/mail.log)."

#. Type: boolean
#. Description
#: ../mailgraph.templates:3001
msgid "Ignore mail to/from localhost?"
msgstr "Ignorare le mail da/per localhost?"

#. Type: boolean
#. Description
#: ../mailgraph.templates:3001
msgid ""
"When using a content filter like amavis, incoming mail is counted more than "
"once, which will result in wrong values. If you use some content filter, you "
"should choose this option."
msgstr ""
"Se si usano dei content filter come amavis, le mail in arrivo sono contate "
"pi? di una volta e quindi si ottengono dei valori sbagliati. Se si usano dei "
"content filter si deve abilitare questa opzione."

#~ msgid "Count incoming mail as outgoing mail?"
#~ msgstr "Contare le mail in arrivo come mail in uscita?"
